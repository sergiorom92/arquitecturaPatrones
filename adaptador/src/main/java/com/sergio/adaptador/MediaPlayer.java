package com.sergio.adaptador;

/**
 *
 * @author sergio
 */
public interface MediaPlayer {
   public void play(String audioType, String fileName);
}
