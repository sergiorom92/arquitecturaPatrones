package com.sergio.singleton;

/**
 *
 * @author sergio
 */
public class Singleton {

    private static Singleton instancia;

    private Singleton() {

    }

    public static synchronized Singleton getInstancia() {
        if (instancia == null) {
            instancia = new Singleton();
        }

        return instancia;
    }

    public void saludar() {
        System.out.println("Hola mundo, soy Singleton");
    }
}
