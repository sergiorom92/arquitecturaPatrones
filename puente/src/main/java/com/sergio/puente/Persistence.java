/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.puente;

/**
 *
 * @author sergio
*/

public interface Persistence {

    /**
     * @param object
     * @return returns objectID
     */
    public String persist(Object object);

    /**
     *
     * @param objectId
     * @return persisted Object
     */
    public Object findById(String objectId);

    /**
     *
     * @param id
     */
    public void deleteById(String id);

}
