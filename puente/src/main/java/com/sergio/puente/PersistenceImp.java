/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.puente;

/**
 *
 * @author sergio
 */
/**
 * Abstraction Imp
 */
public class PersistenceImp implements Persistence {

    private PersistenceImplementor implementor = null;

    public PersistenceImp(PersistenceImplementor imp) {

        this.implementor = imp;

    }

    @Override
    public void deleteById(String id) {

        implementor.deleteObject(Long.parseLong(id));

    }

    @Override
    public Object findById(String objectId) {

        return implementor.getObject(Long.parseLong(objectId));
    }

    @Override
    public String persist(Object object) {

        return Long.toString(implementor.saveObject(object));

    }

}
