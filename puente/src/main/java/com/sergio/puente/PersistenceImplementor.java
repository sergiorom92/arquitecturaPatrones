/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.puente;


/**
 * Implementor Interface
 */
public interface PersistenceImplementor {

    public long saveObject(Object object);

    public void deleteObject(long objectId);

    public Object getObject(long objectId);

}
