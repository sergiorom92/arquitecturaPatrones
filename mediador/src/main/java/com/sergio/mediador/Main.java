/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.mediador;

/**
 *
 * @author sergio
 */
public class Main {
    public static void main(String[] args) {
        SalonDeChat s = new SalonDeChat();
        
        Usuario u = new Usuario(s);
        u.setNombre("Sergio");
        s.registra(u);
        
        Usuario u1 = new Usuario(s);
        u1.setNombre("Camilo");
        s.registra(u1);
        
        Usuario u2 = new Usuario(s);
        u2.setNombre("Pepe");
        s.registra(u2);
        
        u.envia("Pepe", "Buenas tardes");
        u1.envia("Sergio", "Cómo está??");
        u2.envia("Francisco", "Francisco?");
        
        
    }
}
