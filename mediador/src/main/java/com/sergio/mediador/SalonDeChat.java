/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.mediador;

import java.util.HashMap;

/**
 *
 * @author sergio
 */
public class SalonDeChat implements ISalonDeChat {

    private HashMap<String, Usuario> participantes = new HashMap<>();

    @Override
    public void registra(Usuario participante) {
        participantes.put(participante.getNombre(), participante);
    }

    @Override
    public void envia(String de, String a, String msg) {
        if (participantes.containsKey(de) && participantes.containsKey(a)) {
            Usuario u = participantes.get(a);
            u.recibe(de, msg);
        }
        else{
            System.out.println("Usuario inexistente");
        }
    }

}
