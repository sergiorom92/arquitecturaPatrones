/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.mediador;

/**
 *
 * @author sergio
 */
public interface ISalonDeChat {

    public abstract void registra(Usuario participante);

    public abstract void envia(String de, String a, String msg);

}
