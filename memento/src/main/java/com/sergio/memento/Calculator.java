/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.memento;

/**
 *
 * @author sergio
 */
/**
 * Originator Interface
 */
public interface Calculator {

    // Create Memento 
    public PreviousCalculationToCareTaker backupLastCalculation();

    // setMemento
    public void restorePreviousCalculation(PreviousCalculationToCareTaker memento);

    // Actual Services Provided by the originator 
    public int getCalculationResult();

    public void setFirstNumber(int firstNumber);

    public void setSecondNumber(int secondNumber);
}
