/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.constructor;

/**
 *
 * @author sergio
 */
public abstract class TextConverter{
	abstract void convertCharacter(char c);
	abstract void convertParagraph();
}

