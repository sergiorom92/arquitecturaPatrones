/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.constructor;



/**
 *
 * @author sergio
 */
//Client
public class Main {

    void createASCIIText(Document doc) {
        ASCIIConverter asciiBuilder = new ASCIIConverter();
        RTFReader rtfReader = new RTFReader(asciiBuilder);
        rtfReader.parseRTF(doc);
        ASCIIText asciiText = asciiBuilder.getResult();
    }

    public static void main(String args[]) {
        Main client = new Main();
        Document doc = new Document();
        client.createASCIIText(doc);
        System.out.println("This is an example of Builder Pattern");
        System.out.println(doc.toString());
    }
}
