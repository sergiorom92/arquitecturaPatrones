/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.constructor;

/**
 *
 * @author sergio
 */
//Director
public class RTFReader {

    private static final char EOF = '0'; //Delimitor for End of File
    final char CHAR = 'c';
    final char PARA = 'p';
    char t;
    TextConverter builder;

    RTFReader(TextConverter obj) {
        builder = obj;
    }

    void parseRTF(Document doc) {
        while ((t = doc.getNextToken()) != EOF) {
            switch (t) {
                case CHAR:
                    builder.convertCharacter(t);
                case PARA:
                    builder.convertParagraph();
            }
        }
    }
}
