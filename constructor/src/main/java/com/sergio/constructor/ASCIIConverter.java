/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.constructor;

/**
 *
 * @author sergio
 */
//Concrete Builder
public class ASCIIConverter extends TextConverter {

    ASCIIText asciiTextObj;//resulting product

    /*converts a character to target representation and appends to the resulting object*/
    @Override
    void convertCharacter(char c) {
        char asciiChar = c;
        //gets the ascii character
        asciiTextObj.append(asciiChar);
    }

    void convertParagraph() {
    }

    ASCIIText getResult() {
        return asciiTextObj;
    }
}
