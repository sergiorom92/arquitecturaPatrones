/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.comando;

/**
 *
 * @author sergio
 */
//ConcreteCommand Class.
public class BuyStockOrder implements Order {

    private StockTrade stock;

    public BuyStockOrder(StockTrade st) {
        stock = st;
    }

    public void execute() {
        stock.buy();
    }
}
