/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergio.comando;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author sergio
 */
// Invoker.
class Agent {
    private Queue<Order> ordersQueue = new LinkedList<>();

    public Agent() {
    }
    
    void placeOrder(Order order) {
        ordersQueue.add(order);
        ordersQueue.poll().execute();
    }    
}
